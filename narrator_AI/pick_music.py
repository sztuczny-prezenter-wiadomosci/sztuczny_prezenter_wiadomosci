# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 11:28:17 2022

@author: weron
"""

import os
from pydub import AudioSegment
import wave
import contextlib
import math
import numpy as np
import soundfile as sf


def pick_music (sentiment, video_length):
    '''Function chooses background music based on sentiment of the text
    and fits it to the length of the video'''
    
    if sentiment>0:
        audio_name = 'news_happy.wav'
    else:
        audio_name = 'news_sad.wav'
    
    
    audio_path = os.path.join(os.path.dirname(__file__), audio_name)
    
    #change the length
    
    with contextlib.closing(wave.open(audio_name,'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate) #seconds
        
    if duration> video_length:  #in seconds
    
        t_end = video_length*1000 #miliseconds
        new_audio = AudioSegment.from_wav(audio_name)
        new_audio = new_audio[0:t_end]
        new_audio.export('news_music.wav', format='wav')
        new_audio_path = os.path.join(os.path.dirname(__file__), "news_music.wav")
    else:
        data, rate = sf.read(audio_name)
        n = math.ceil(video_length*rate/len(data))
        new_audio = np.tile(data, (n,1)) #two channels
        sf.write('news_music.wav', new_audio, rate)
        new_audio_path = os.path.join(os.path.dirname(__file__), "news_music.wav")
        
        
    print("Done")              
    return new_audio_path


a_p = pick_music(-0.4, 28)

