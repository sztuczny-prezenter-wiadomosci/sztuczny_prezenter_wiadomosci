import narrator_AI
import os
import re


def test_import():
    """Package testing stub."""
    assert narrator_AI.TEST_VAR == 1


def test_text_to_speech():
    for g in ["f", "m"]:
        for s, name in zip([-0.5, 0.5], ["sad", "happy"]):
            txt_path = os.path.join(os.path.dirname(__file__), 'text.txt')
            path = os.path.join(os.path.dirname(__file__), f'{g}_{name}.wav')
            data = narrator_AI.DataCollector(txt_path)
            data.sentiment = s
            narrator = narrator_AI.Narration(data)
            narrator.text2speech(path, g)

            assert os.path.exists(path)


def test_narration():
    txt_path = os.path.join(os.path.dirname(__file__), 'text.txt')
    data = narrator_AI.DataCollector(txt_path)
    narrator = narrator_AI.Narration(data)
    path = os.path.join(os.path.dirname(__file__),
                        f'sentiment_narration.wav')
    narrator.text2speech(path)
    assert os.path.exists(path)


def test_add_audio():
    audio_path = os.path.join(os.path.dirname(__file__), 'test.wav')
    video_path = os.path.join(os.path.dirname(__file__), 'news_0_1_cut.mp4')
    path = os.path.join(os.path.dirname(__file__), 'test_audio_anim.mp4')
    narrator_AI.add_audio(video_path, audio_path, path)
    assert os.path.exists(path)


def test_frame_animation():
    # df_path = os.path.join(os.path.dirname(__file__), 'test.csv')
    # print(os.listdir(os.path.dirname(__file__)))
    # frames = [f for f in os.listdir(os.path.dirname(__file__)) if re.match("pogodynka_" + ".*\.jpg$", f)]
    # print(frames)
    # frames = [os.path.join(os.path.dirname(__file__), 'pogodynka_a.jpg'),os.path.join(os.path.dirname(__file__), 'pogodynka_f_w.jpg')]
    # path_af = os.path.join(os.path.dirname(__file__), 'shorttest.wav')
    print(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "test", "test.csv"))
    narrator_AI.frame_anim(test=1)

def test_text_sentiment():
    text_path = os.path.join(os.path.dirname(__file__), 'text.txt')
    most_common, compound = narrator_AI.sentiment_analysis(text_path)
    assert compound != 0

def test_download_pictures():
    keyword = "keyword"
    url = f'https://unsplash.com/s/photos/{keyword}'
    count = narrator_AI.photo_downloader(url, '{keyword}')
    path = os.path.join(os.path.dirname(__file__), 'pictures')
    assert count>1

def test_composing_videos():
    vid1_path = os.path.join(os.path.dirname(__file__), 'audio_break.mp4')
    vid2_path = os.path.join(os.path.dirname(__file__), 'testnews2.mp4')
    output_path = os.path.join(os.path.dirname(__file__), 'composed.mp4')
    narrator_AI.concatenate_vid([vid2_path,vid1_path], output_path, method="compose")
    assert os.path.exists(output_path)

